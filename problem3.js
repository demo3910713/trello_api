const http = require("https");
let APIToken =
  "ATTA3b4afa91e29b410cdb9b58015ba52d7e5bea261bd6505960f5991fb019282247A3A85651";
let APIKey = "7d5923b2fb49db8f1a0d7ace94e5dc29";

function getListByBoardId(id) {
  let promise = new Promise((resolve, reject) => {
    let path = `https://api.trello.com/1/boards/${id}/lists?key=${APIKey}&token=${APIToken}`;
    const req = http.request(path, (res) => {
      res.setEncoding("utf-8");

      res.on("data", (chunk) => {
        resolve(chunk);
      });
    });

    req.on("error", (error) => {
      reject(error);
    });

    req.end();
  });
  return promise;
}

module.exports = getListByBoardId;
