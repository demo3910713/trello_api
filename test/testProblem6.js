const { createBoard, createList, createcards } = require("../problem6");
let name = "Radha";
let numOfList = 3;
function main(name, numOfList) {
  createBoard(name)
    .then((data) => {
      let listPromises = [];
      for (let index = 1; index <= numOfList; index++) {
        listPromises.push(createList("random" + index, data));
      }
      return Promise.all(listPromises);
    })
    .then((data1) => {
      let allPromises = [];
      data1.forEach((element) => {
        allPromises.push(createcards("chandra", element));
      });
      return Promise.all(allPromises);
    })
    .then(console.log)
    .catch((error) => {
      console.error(error);
    });
}

main(name, numOfList);
