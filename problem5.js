const http = require('https');
let APIToken='ATTA3b4afa91e29b410cdb9b58015ba52d7e5bea261bd6505960f5991fb019282247A3A85651'
let APIKey = '7d5923b2fb49db8f1a0d7ace94e5dc29'

function getListByBoardId(id){
    let promise = new Promise((resolve, reject)=>{
      let path = `https://api.trello.com/1/boards/${id}/lists?key=${APIKey}&token=${APIToken}`
      const req = http.request(path, (res) => {
          res.setEncoding('utf-8')  
        
        res.on('data', (chunk) => {
           let data  = JSON.parse(chunk)
           for (let index = 0; index < data.length; index++) {
            const element = data[index];
            resolve(getCardsByListId(element.id))
           }
        });

        res.on('error', ()=>{
           
        })
      });
      
      req.on('error', (error) => {
        reject(error)
      });
      
      req.end();
 })
   return promise 
}

function getCardsByListId(id){
        let promise = new Promise((resolve, reject)=>{
          let path = `https://api.trello.com/1/lists/${id}/cards?key=${APIKey}&token=${APIToken}`
            const req = http.request(path, (res) => {
              res.setEncoding('utf-8') 
                res.on('data', (chunk) => {
                  let data=JSON.parse(chunk)
                  resolve(data)
                });
              
                res.on('end', () => {
                   
                });
              });
              
              req.on('error', (error) => {
                reject(error)
              });
              
              req.end();
         })
       return promise 
    
    }
 

module.exports = getListByBoardId
