const createBoard = require("../problem2");

const boardName = "chandra";

function main(boardName) {
  createBoard(boardName)
    .then((board) => {
      console.log("Board created:", board);
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

main(boardName);
