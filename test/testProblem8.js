const { deletelist, getListByBoardId } = require("../problem7");
const id = "gJU5Tk03";

function main(id) {
  getListByBoardId(id)
    .then((data) => {
      let index = 0;
      function processNext() {
        if (index < data.length) {
          const element = data[index++];
          console.log(element.id);
          return deletelist(element.id).then(processNext());
        } else {
          return Promise.resolve();
        }
      }

      return processNext();
    })
    .then(() => {
      console.log("All promises executed sequentially.");
    })
    .catch((err) => {
      console.log(err);
    });
}

main(id);
