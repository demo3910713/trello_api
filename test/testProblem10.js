const getListByBoardId = require("../problem5");
const { getCheckItems, updateStatus } = require("../problem10");

let id = "QzAkNN4V";

function main(id) {
  getListByBoardId(id)
    .then((data) => {
      let promisesChecklists = [];
      data.forEach(element => {
          promisesChecklists.push(getCheckItems(element.id));
      });
      return Promise.all(promisesChecklists);
    })
    .then((data) => {
      let promisesUpdateStatus = [];
      data.forEach((element) => {
        const values = JSON.parse(element);
        promisesUpdateStatus.push(processChecklistItems(values, 0));
      });
      return Promise.all(promisesUpdateStatus);
    })
    .then(() => {
      console.log("All checklist items updated ");
    })
    .catch((error) => console.error(error));
}


function processChecklistItems(checklistItems, index) {
  if (index >= checklistItems.length) {
    return Promise.resolve();
  }

  const element = checklistItems[index];
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      updateStatus(element.idCard, element.checkItems[0].id)
        .then(() => {
          resolve(processChecklistItems(checklistItems, index + 1));
          
        })
        .catch(reject);
    }, 1000);
  });
}

main(id);
