const http = require("https");
let APIToken =
  "ATTA3b4afa91e29b410cdb9b58015ba52d7e5bea261bd6505960f5991fb019282247A3A85651";
let APIKey = "7d5923b2fb49db8f1a0d7ace94e5dc29&";
const options = {
  method: "PUT",
};

function deletelist(id) {
  let promise = new Promise((resolve, reject) => {
    const url = `https://api.trello.com/1/lists/${id}/closed?key=${APIKey}&token=${APIToken}&value=true`;
    const req = http.request(url, options, (res) => {
      res.setEncoding("utf-8");
      res.on("data", (d) => {
        let data = d;
        resolve(data);
      });
    });

    req.on("error", (error) => {
      console.error(error);
    });

    req.end();
  });
  return promise;
}

function getListByBoardId(id) {
  try {
    let promise = new Promise((resolve, reject) => {
      let path = `https://api.trello.com/1/boards/${id}/lists?key=${APIKey}&token=${APIToken}`;
      const req = http.request(path, (res) => {
        res.setEncoding("utf-8");

        res.on("data", (chunk) => {
          let data = JSON.parse(chunk);
          resolve(data);
        });
      });

      req.on("error", (error) => {
        reject(error);
      });

      req.end();
    });
    return promise;
  } catch (error) {
    return error;
  }
}

module.exports = { getListByBoardId, deletelist };
