const getListByBoardId = require("../problem5");
const { getCheckItems, updateStatus } = require("../problem9");
let id = "QzAkNN4V";
function main(path) {
  getListByBoardId(path)
    .then((data) => {
      let promisesOfCards = [];
      for (let index = 0; index < data.length; index++) {
        const element = data[index];
        promisesOfCards.push(getCheckItems(element.id));
      }
      return Promise.all(promisesOfCards);
    })
    .then((data) => {
      let promises = [];
      data.forEach((element1) => {
        let values = JSON.parse(element1);
        values.forEach((element) => {
          promises.push(updateStatus(element.idCard, element.checkItems[0].id));
        });
      });
      return Promise.all(promises);
    })
    .then(() => console.log("All checklist items updated"))

    .catch((error) => {
      console.error(error);
    });
}

main(id);
