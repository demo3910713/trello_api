const https = require("https");
let APIToken =
  "ATTA3b4afa91e29b410cdb9b58015ba52d7e5bea261bd6505960f5991fb019282247A3A85651";
let APIKey = "7d5923b2fb49db8f1a0d7ace94e5dc29&";
const options = {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
  },
};
function createBoard(name) {
  let promise = new Promise((resolve, reject) => {
    let path = `https://api.trello.com/1/boards/?name=${name}&key=${APIKey}&token=${APIToken}`;
    let req = https.request(path, options, (res) => {
      res.setEncoding("utf-8");
      res.on("data", (chunk) => {
        resolve(chunk);
      })
    })
    req.on("error", (error) => {
      reject(error);
    });
    req.write(JSON.stringify(options));
    req.end();
  });
  return promise;
}
module.exports = createBoard;
