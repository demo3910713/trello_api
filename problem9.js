
let APIToken='ATTA3b4afa91e29b410cdb9b58015ba52d7e5bea261bd6505960f5991fb019282247A3A85651'
let APIKey = '7d5923b2fb49db8f1a0d7ace94e5dc29'


function getCheckItems(id) {

 return fetch(`https://api.trello.com/1/cards/${id}/checklists?key=${APIKey}&token=${APIToken}`,{
  method: 'GET'
})
  .then(response => {
    return response.text();
  })
}

function updateStatus(id, idCheckItem){
  return new Promise((resolve, reject)=>{
   fetch(`https://api.trello.com/1/cards/${id}/checkItem/${idCheckItem}?state=incomplete&key=${APIKey}&token=${APIToken}`, {
  method: 'PUT'
})
  .then(response => {
    if (response.status === 200) {
      console.log(` checklist item updated `)
      resolve(response.text())
    }else{
      reject(`Response: ${response.status} ${response.statusText}`)
    }
  })
})
}
module.exports = {getCheckItems, updateStatus}
   