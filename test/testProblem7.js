const {deletelist, getListByBoardId} = require('../problem7')

const id ='Be27D8EN'

function main(id) {
    getListByBoardId(id).then((data)=>{
        let promises =[]
       data.forEach(element => {
          promises.push(deletelist(element.id))
      });
     return Promise.all(promises)
    }).then(console.log)
    .catch((err)=>{
        console.log(err)
    })
}

main(id)